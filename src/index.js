const prisma = require('./db');

const createReport = async ({ schoolId, reportName }) => {
    try{
        const report = await prisma.$transaction(async () => {
            // get current report number
            const school = await prisma.school.findUnique({
                where: {
                    id: schoolId
                }
            });
            const newIncrementalReportNumber = school.incrementalReportNumber + 1;
            console.log("newIncrementalReportNumber", newIncrementalReportNumber);

            // Increase incrementalReportNumber
            await prisma.school.update({
                where: { id: schoolId },
                data: {
                    incrementalReportNumber: newIncrementalReportNumber
                }
            });
    
            // Create report
            const newReport = await prisma.report.create({
                data: {
                    publicId: newIncrementalReportNumber,
                    schoolId: school.id,
                    name: reportName,
                },
            });
    
            // Return
            return newReport;
        });
        return report;
    }catch(err){
        console.error("Error in createReport")
        console.error(err);
        throw err;
    }
}

const batchCreateReport = async () => {
    try{
        const totalQuery = 2;
        const arrayIterator = new Array(totalQuery).fill(null);
        await Promise.all(
            arrayIterator.map(() => {
                return createReport({schoolId: "1", reportName: 'New Report'});
            })
        );
    
    }catch(err){
        console.error("Error in batchCreateReport")
        console.error(err);
        throw err;
    }
}

batchCreateReport();



