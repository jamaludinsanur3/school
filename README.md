## Database Issue

To run the project and replicate the issue :


set env DATABASE_URL as connection string to your database

i.e : 
```
DATABASE_URL=postgresql://postgres:password@localhost:5432/school
or
DATABASE_URL=mysql://username:password@localhost:3306/school
```

How to run
```
// install dependency
npm i

// generate database structure
npm run prisma:migrate

// generate database data
npm run prisma:reset

// run the code
npm run dev
```

The issue :
Concurrent create data fail with following error.
```
Unique constraint failed on the fields: (`publicId`,`schoolId`)
```
