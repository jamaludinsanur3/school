const prisma = require('../src/db');

async function main() {
    await prisma.school.create({
        data: {
            id: "1",
            name: "Test School",
            incrementalReportNumber: 1000,
        }
    })
}

main()
.then( () => {
    console.log("Finish generate seeds")
})
.catch(err => {
    console.error("Error generate seeds");
    console.error(err);
})